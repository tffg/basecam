#include <ros/ros.h>
#include <ros/timer.h>
#include <ros/console.h>

#include <sensor_msgs/Imu.h>

#include <boost/asio.hpp>
#include <boost/asio/serial_port.hpp>
#include <boost/system/error_code.hpp>
#include <boost/system/system_error.hpp>
#include <boost/bind.hpp>
#include <boost/thread.hpp>

#include <Eigen/Geometry>
#include <Eigen/Dense>

#define SERIAL_PORT_READ_BUF_SIZE 256
#define SBGC_CMD_MAX_BYTES 255
#define SBGC_CMD_NON_PAYLOAD_BYTES 5
#define SBGC_RC_NUM_CHANNELS 6
#define SBGC_CMD_DATA_SIZE (SBGC_CMD_MAX_BYTES - SBGC_CMD_NON_PAYLOAD_BYTES)
#define SBGC_IMU_TO_DEG 0.02197265625
#define SBGC_IMU_TO_RAD (SBGC_IMU_TO_DEG/180.0*M_PI)

#define CMD_REALTIME_DATA_3       23
#define CMD_REALTIME_DATA_4       25
#define CMD_DATA_STREAM_INTERVAL  85
#define CMD_REALTIME_DATA_CUSTOM  88
#define RT_DATA_CUSTOM_IMU_ANGLE      1<<0
#define RT_DATA_CUSTOM_TARGET_ANGLE   1<<1
#define RT_DATA_CUSTOM_TARGET_SPEED   1<<2
#define RT_DATA_CUSTOM_STATOR_ANGLE   1<<3
#define RT_DATA_CUSTOM_GYRO           1<<4
#define RT_DATA_CUSTOM_RC             1<<5
#define RT_DATA_CUSTOM_ATT_MATRIX     1<<6
#define RT_DATA_CUSTOM_RC_PPM         1<<7
#define RT_DATA_CUSTOM_ACC            1<<8
//#define RT_DATA_CUSTOM  RT_DATA_CUSTOM_IMU_ANGLE | RT_DATA_CUSTOM_GYRO | RT_DATA_CUSTOM_ACC

enum STATE
{
  STATE_WAIT, STATE_GOT_MARKER, STATE_GOT_ID, STATE_GOT_LEN, STATE_GOT_HEADER, STATE_GOT_DATA
};

typedef boost::shared_ptr<boost::asio::serial_port> serial_port_ptr;


// CMD_REALTIME_DATA_3, CMD_REALTIME_DATA_4
typedef struct __attribute__((aligned)) {
  struct {
    int16_t acc_data;
    int16_t gyro_data;
  } sensor_data[3];  // ACC and Gyro sensor data (with calibration) for current IMU (see cur_imu field)
  uint16_t serial_error_cnt; // counter for communication errors
  uint16_t system_error; // system error flags, defined in SBGC_SYS_ERR_XX 
  uint8_t reserved1[4];
  int16_t rc_raw_data[SBGC_RC_NUM_CHANNELS]; // RC signal in 1000..2000 range for ROLL, PITCH, YAW, CMD, EXT_ROLL, EXT_PITCH channels
  int16_t imu_angle[3]; // ROLL, PITCH, YAW Euler angles of a camera, 16384/360 degrees
  int16_t frame_imu_angle[3]; // ROLL, PITCH, YAW Euler angles of a frame, if known
  int16_t target_angle[3]; // ROLL, PITCH, YAW target angle
  uint16_t cycle_time_us; // cycle time in us. Normally should be 800us
  uint16_t i2c_error_count; // I2C errors counter
  uint8_t reserved2;
  uint16_t battery_voltage; // units 0.01 V
  uint8_t state_flags1; // bit0: motor ON/OFF state;  bits1..7: reserved
  uint8_t cur_imu; // actually selecteted IMU for monitoring. 1: main IMU, 2: frame IMU
  uint8_t cur_profile; // active profile number starting from 0
  uint8_t motor_power[3]; // actual motor power for ROLL, PITCH, YAW axis, 0..255
  
  // Fields below are filled only for CMD_REALTIME_DATA_4 command
  int16_t rotor_angle[3]; // relative angle of each motor, 16384/360 degrees
  uint8_t reserved3;
  int16_t balance_error[3]; // error in balance. Ranges from -512 to 512,  0 means perfect balance.
  uint16_t current; // Current that gimbal takes, in mA.
  int16_t magnetometer_data[3]; // magnetometer sensor data (with calibration)
  int8_t  imu_temp_celcius;  // temperature measured by the main IMU sensor, in Celsius
  int8_t  frame_imu_temp_celcius;  // temperature measured by the frame IMU sensor, in Celsius
  uint8_t reserved4[38];
} SBGC_cmd_realtime_data_t;

// CMD_REALTIME_DATA_CUSTOM
typedef struct __attribute__((aligned)) {
  uint16_t  timestamp;        // Timestamp in milliseconds
  int16_t   imu_angle[3];    // Main IMU Angles (Euler)  LSB=0.02197265625 deg
  int16_t   target_angle[3]; // Target angles that gimbal should keep (Euler) LSB=0.02197265625 deg
  int16_t   target_speed[3];  // Target speed that gimbal should keep over Euler axes LSB=0.06103701895 deg/sec
  int16_t   stator_rotor_angle[3];  // Relative angle of joints (motors) LSB = 0.02197265625 deg
  int16_t   gyro_data[3];     // Gyro sensor data after calibrations applied
  int16_t   rc_data[6] ;      // RC data in high resolution: ROLL, PITCH, YAW, CMD, FC_ROLL, FC_PITCH
  float     z1_vector[3];     // IMU attitude in a form of a rotation matrix
  float     h1_vector[3];
  int16_t   rc_channels[18];  // All RC channels captured from s-bus, spektrum or Sum-PPM inputs
  int16_t   acc_data[3];      // Accelerometer sensor data with calibration
}SBGC_cmd_realtime_data_custom_t;

class SerialCommand {
public:
  uint8_t pos;
  uint8_t id;
  uint8_t data[SBGC_CMD_DATA_SIZE];
  uint8_t len;

  void init(uint8_t _id) {
    id = _id;
    len = 0;
    pos = 0;
  }
};

class Rs232COM
{
protected:
  boost::asio::io_service io_service_;
  serial_port_ptr port_;
  boost::mutex mutex_;
  
  std::string portName;
  int baud;

  char read_buf_raw_[SERIAL_PORT_READ_BUF_SIZE];
  std::string read_buf_str_;

  char end_of_line_char_;

  SerialCommand cmd_in, cmd_out;
  int len;
  uint8_t checksum;
  enum STATE state;

  /*uint8_t data_buffer[SBGC_CMD_MAX_BYTES];
  uint8_t cmd;
  uint8_t checksum;
  int len;
  int idx;
  enum STATE state;
  bool data_valid;*/
  
public:

  ros::Publisher imu_pub;

  Rs232COM(): portName("/dev/ttyUSB0"), baud(115200), checksum(0), state(STATE_WAIT), end_of_line_char_('>'){};//: my_io(), serial(my_io), portName("/dev/ttyUSB2"), started(false){};
  /** 
   * \brief Initialisation function, this function must be call before listen or write
   * \param portName rs232 port name to communicate with
   */

  ros::Timer timer;

  void init(int argc, char** argv)
  {

    ros::init(argc, argv, "basecam");
    ros::NodeHandle n("~");

    imu_pub = n.advertise<sensor_msgs::Imu>("imu", 1);

    if (argc > 1)
      portName = std::string(argv[1]);

    if(argc > 2)
      baud = atoi(argv[2]);
    
    //open serial
    if (port_) {
      std::cout << "error : port is already opened..." << std::endl;
      ros::shutdown();
    }

    start();

    if(1)
      timer = n.createTimer(ros::Duration(0.01), &Rs232COM::timerCallback, this);
    else{
      cmd_out.id = CMD_DATA_STREAM_INTERVAL;
      cmd_out.len = 13;
      memset(cmd_out.data, 0, cmd_out.len);
      cmd_out.data[0] = CMD_REALTIME_DATA_CUSTOM;
      cmd_out.data[2] = 10;
      cmd_out.data[3] = 255;
      cmd_out.data[4] = 1;

      send_cmd();
    }

    ros::MultiThreadedSpinner spinner(4);
    spinner.spin();

    stop();

    return;
  }

  void start(){
    boost::system::error_code ec;

    port_ = serial_port_ptr(new boost::asio::serial_port(io_service_));
    port_->open(portName, ec);
    if (ec) {
      std::cout << "error opening port : "
        << portName << " ( " << ec.message().c_str() << ")" << std::endl; 
      ros::shutdown();
    }

    // option settings...
    port_->set_option(boost::asio::serial_port_base::baud_rate(baud));
    port_->set_option(boost::asio::serial_port_base::character_size(8));
    port_->set_option(boost::asio::serial_port_base::stop_bits(boost::asio::serial_port_base::stop_bits::one));
    port_->set_option(boost::asio::serial_port_base::parity(boost::asio::serial_port_base::parity::none));
    port_->set_option(boost::asio::serial_port_base::flow_control(boost::asio::serial_port_base::flow_control::none));

    async_read_some_();

    // boost::asio::service should be launched after async_read_some so it has some work when launched preventing to return    
    boost::thread t(boost::bind(&boost::asio::io_service::run, &io_service_));
  }
  
  void async_read_some_()
  {
    if (port_.get() == NULL || !port_->is_open()) return;

    port_->async_read_some( 
      boost::asio::buffer(read_buf_raw_, SERIAL_PORT_READ_BUF_SIZE),
      boost::bind(
        &Rs232COM::on_receive_,
        this, boost::asio::placeholders::error, 
        boost::asio::placeholders::bytes_transferred));
  }

  /** \brief read loop on serial port
   *  \details this function read data on serial port and send every good frame to ds301 stack
   *  Warning : - this is a blocking function
   *            - init must be call before calling this function
   */
  void on_receive_(const boost::system::error_code& ec, size_t bytes_transferred)
  {
    boost::mutex::scoped_lock lock(mutex_);

    if (port_.get() == NULL || !port_->is_open()) return;
    if (ec) {
      async_read_some_();
      return;
    }

    for (unsigned int i = 0; i < bytes_transferred; ++i) {
      char c = read_buf_raw_[i];
      if (c == end_of_line_char_) {
        parseData(read_buf_str_);
        read_buf_str_.clear();
      }
      //else {
        read_buf_str_ += c;
      //}
    }

    async_read_some_();
  }

  void parseData(const std::string &data){

    if(data.size() == 0)
        return;

    //std::cout << data << " (" << data.size() << ")" << std::endl;

    uint8_t c;
    for(int i = 0; i < data.size(); i++){
      c = data.at(i);
      switch(state)
      {
        case STATE_WAIT:
          if(c == '>'){
            state = STATE_GOT_MARKER;
            ROS_DEBUG_STREAM("GOT_MARKER" << std::endl);
          }
          break;

        case STATE_GOT_MARKER:
          cmd_in.init(c);
          state = STATE_GOT_ID;
          ROS_DEBUG_STREAM("GOT_ID" << std::endl);
          break;

        case STATE_GOT_ID:
          len = c;
          state = STATE_GOT_LEN;
          ROS_DEBUG_STREAM("GOT_LEN" << std::endl);
          break;

        case STATE_GOT_LEN:
          if(c == (uint8_t)(cmd_in.id+len) && len <= sizeof(cmd_in.data)){
            checksum = 0;
            state = (len == 0) ? STATE_GOT_DATA : STATE_GOT_HEADER;
            if(len == 0)
              ROS_DEBUG_STREAM("GOT_DATA" << std::endl);
            else
              ROS_DEBUG_STREAM("GOT_HEADER" << std::endl);
          }else{
            state = STATE_WAIT;
            ROS_DEBUG_STREAM("WRONG HEADER CHECKSUM" << std::endl);
          }

          break;

        case STATE_GOT_HEADER:
          cmd_in.data[cmd_in.len++] = c;
          checksum+=c;
          if(cmd_in.len == len){
              state = STATE_GOT_DATA;
              ROS_DEBUG_STREAM("GOT_DATA" << std::endl);
            }
          break;

        case STATE_GOT_DATA:
          state = STATE_WAIT;
          if(c != checksum){
            ROS_DEBUG_STREAM("WRONG DATA CHECKSUM" << std::endl);
            return;
          }else
            ROS_DEBUG_STREAM("DATA CHECKSUM VERIFIED" << std::endl);
          break;
      }
    }

    if(state == STATE_WAIT){

      if(cmd_in.id == 23 || cmd_in.id == 25){

        SBGC_cmd_realtime_data_t rt_data;
        SBGC_cmd_realtime_data_unpack(rt_data, cmd_in);

        Eigen::Matrix3f Rot;
        Rot = Eigen::AngleAxisf(rt_data.imu_angle[0]*SBGC_IMU_TO_RAD, Eigen::Vector3f::UnitX()) * \
              Eigen::AngleAxisf(rt_data.imu_angle[1]*SBGC_IMU_TO_RAD, Eigen::Vector3f::UnitY()) * \
              Eigen::AngleAxisf(rt_data.imu_angle[2]*SBGC_IMU_TO_RAD, Eigen::Vector3f::UnitZ());
        Eigen::Quaternionf quat(Rot);

        sensor_msgs::Imu msg;

        msg.header.stamp = ros::Time::now();
        msg.angular_velocity.x = rt_data.sensor_data[0].gyro_data/32767.0*2000.0*M_PI/180.0;
        msg.angular_velocity.y = rt_data.sensor_data[1].gyro_data/32767.0*2000.0*M_PI/180.0;
        msg.angular_velocity.z = rt_data.sensor_data[2].gyro_data/32767.0*2000.0*M_PI/180.0;
        msg.linear_acceleration.x = rt_data.sensor_data[0].acc_data/512.0*9.81;
        msg.linear_acceleration.y = rt_data.sensor_data[1].acc_data/512.0*9.81;
        msg.linear_acceleration.z = rt_data.sensor_data[2].acc_data/512.0*9.81;
        msg.orientation.x = quat.x();
        msg.orientation.y = quat.y();
        msg.orientation.z = quat.z();
        msg.orientation.w = quat.w();

        imu_pub.publish(msg);
      }else{ 
        
        if(cmd_in.id == 88){

          SBGC_cmd_realtime_data_custom_t rt_data;
          SBGC_cmd_realtime_data_custom_unpack(rt_data, cmd_in);

          Eigen::Matrix3f Rot;
          Rot = Eigen::AngleAxisf(rt_data.imu_angle[0]*SBGC_IMU_TO_RAD, Eigen::Vector3f::UnitX()) * \
                Eigen::AngleAxisf(rt_data.imu_angle[1]*SBGC_IMU_TO_RAD, Eigen::Vector3f::UnitY()) * \
                Eigen::AngleAxisf(rt_data.imu_angle[2]*SBGC_IMU_TO_RAD, Eigen::Vector3f::UnitZ());
          Eigen::Quaternionf quat(Rot);

          std::ostringstream ss;
          ss << rt_data.timestamp;

          sensor_msgs::Imu msg;

          msg.header.stamp = ros::Time::now();
          msg.header.frame_id = ss.str();
          msg.angular_velocity.x =    rt_data.gyro_data[0]/32767.0*2000.0*M_PI/180.0;
          msg.angular_velocity.y =    rt_data.gyro_data[1]/32767.0*2000.0*M_PI/180.0;
          msg.angular_velocity.z =    rt_data.gyro_data[2]/32767.0*2000.0*M_PI/180.0;
          msg.linear_acceleration.x = rt_data.acc_data[0]/512.0*9.81;
          msg.linear_acceleration.y = rt_data.acc_data[1]/512.0*9.81;
          msg.linear_acceleration.z = rt_data.acc_data[2]/512.0*9.81;
          msg.orientation.x = quat.x();
          msg.orientation.y = quat.y();
          msg.orientation.z = quat.z();
          msg.orientation.w = quat.w();

          imu_pub.publish(msg);

        }else
          ROS_DEBUG("> %d\n", cmd_in.id);
      }
    }
  }

  /*
* Unpacks SerialCommand object to command structure.
* Returns 0 on success, PARSER_ERROR_XX code on fail.
*/
uint8_t SBGC_cmd_realtime_data_unpack(SBGC_cmd_realtime_data_t &p, SerialCommand &cmd) {
    if(cmd.len <= sizeof(p)) {
      memcpy(&p, cmd.data, cmd.len);
      return 0;
    } else
      return 1;
}

  /*
* Unpacks SerialCommand object to command structure.
* Returns 0 on success, PARSER_ERROR_XX code on fail.
*/
uint8_t SBGC_cmd_realtime_data_custom_unpack(SBGC_cmd_realtime_data_custom_t &p, SerialCommand &cmd) {
    if(cmd.len <= sizeof(p)) {
      memcpy(&p, cmd.data, cmd.len);
      return 0;
    } else
      return 1;
}
  
  /** \brief function for writing on serial port
      \details it is called from Effective_Write from DS301 stack
               Warning :  init must be called before use this function
  */
  /*void write(char * st)
  {
      boost::system::error_code ec;
      ROS_DEBUG_STREAM( "write"<< st << std::endl);
      boost::asio::write(serial, boost::asio::buffer(st,strlen(st)),boost::asio::transfer_all(),ec);
      if (ec !=0)
        std::cout << "send error" << std::endl;
  }*/

  int write_some(const std::string &buf)
  {
    return write_some(buf.c_str(), buf.size());
  }

  int write_some(const char *buf, const int &size)
  {
    boost::system::error_code ec;

    if (!port_) return -1;
    if (size == 0) return 0;

    return port_->write_some(boost::asio::buffer(buf, size), ec);
  }

  void stop()
  {
    boost::mutex::scoped_lock lock(mutex_);

    if (port_) {
      port_->cancel();
      port_->close();
      port_.reset();
    }
    io_service_.stop();
    io_service_.reset();
  }

  void send_cmd()
  {
    char data[] = {'>', cmd_out.id, cmd_out.len, cmd_out.id+cmd_out.len};
    std::string data_str(data);

    uint8_t checksum = 0;
    for(int i = 0; i < cmd_out.len; i++){
      data_str.push_back(cmd_out.data[i]);
      checksum+=cmd_out.data[i];
    }
    data_str.push_back(checksum);

    write_some(data_str);
  }

  void timerCallback(const ros::TimerEvent&){

    //SerialCommand cmd;

    cmd_out.id = CMD_REALTIME_DATA_CUSTOM;
    cmd_out.len = 10;
    memset(cmd_out.data, 0, cmd_out.len);
    cmd_out.data[0] = 255;
    cmd_out.data[1] = 1;

    send_cmd();

    //char data[] = {'>', 23, 0, 23, 0, 0};
    //char data[] = {'>', 88, 10, 98, 0xff, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0xff};
    //uint8_t data[] = {0x3E, 0x58, 0x0A, 0x62, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xFF};
    //write_some((char*)data, 6);
    //std::cout << "command sent!" << std::endl;
  }

};


int main(int argc, char **argv){

  Rs232COM rs232;

  rs232.init(argc, argv);
  //rs232.listen();

  return 0;
}
